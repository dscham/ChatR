import type MessageType from "$lib/components/chat/MessageType";
import type ContentType from "$lib/components/chat/ContentType";

export default interface Message {
    id?: string;
    userId?: string;
    messageType: MessageType,
    contentType?: ContentType,
    created: Date;
    content: string;
}