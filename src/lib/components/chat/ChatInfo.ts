import type User from "$lib/components/chat/User";
import type Message from "$lib/components/chat/Message";

export default interface ChatInfo {
    user: User,
    lastMessage?: Message;
}