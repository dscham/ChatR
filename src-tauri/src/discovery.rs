use std::net::{Ipv4Addr, UdpSocket};
use std::str::FromStr;
use std::sync::atomic::AtomicBool;
use std::{thread, time};

use nanoid::nanoid;
use rmp_serde;
use serde::{Deserialize, Serialize};
use tauri::Manager;

use crate::{chat, state, view_model};

const LISTEN_ANY: &str = "0.0.0.0";
const KILL_PACKET: &[u8; 128] = &[0; 128];
const DISCOVERY_PORT: u16 = 42069;
const DUMMY_PORT: u16 = 42043;
const DISCOVERY_MULTICAST_ADDRESS: &str = "224.0.0.1";

static mut RUNNING: AtomicBool = AtomicBool::new(false);

#[derive(Debug, PartialEq, Deserialize, Serialize)]
pub(crate) struct HostInfo {
    pub id: String,
    pub name: String,
}

impl HostInfo {
    pub(crate) fn new(name: &str) -> HostInfo {
        HostInfo {
            id: nanoid!(6),
            name: name.to_string(),
        }
    }
}

pub(crate) fn start<T: tauri::Runtime>(tauri_app_handle: tauri::AppHandle<T>) {
    if !is_running() {
        set_running(true);

        let receive_socket = UdpSocket::bind(format!("{}:{}", LISTEN_ANY, DISCOVERY_PORT))
            .expect("Could not bind UDP socket");

        receive_socket
            .join_multicast_v4(
                &Ipv4Addr::from_str(DISCOVERY_MULTICAST_ADDRESS).unwrap(),
                &Ipv4Addr::UNSPECIFIED,
            )
            .expect("Could not join multicast group");
        // receive_socket
        //     .set_multicast_loop_v4(false)
        //     .expect("Could not set multicast loop");
        let send_socket = receive_socket.try_clone().unwrap();

        thread::spawn(move || receive_discover(receive_socket, tauri_app_handle));
        thread::spawn(move || send_discover(send_socket));
    }
}

pub(crate) fn stop() {
    if is_running() {
        set_running(false);
        send_kill();
    }
}

fn send_kill() {
    let socket = UdpSocket::bind(format!("{}:{}", Ipv4Addr::LOCALHOST, DUMMY_PORT))
        .expect("couldn't bind to address");
    socket
        .connect(format!("{}:{}", Ipv4Addr::LOCALHOST, DISCOVERY_PORT))
        .expect("connect function failed");
    socket.send(KILL_PACKET).expect("couldn't send kill packet");
}

fn receive_discover<T: tauri::Runtime>(socket: UdpSocket, tauri_app_handle: tauri::AppHandle<T>) {
    while is_running() {
        let mut buffer = [0; 1024];
        if let Ok((size, peer_socket)) = socket.peek_from(&mut buffer) {
            let mut buffer = vec![0; size];
            socket
                .recv_from(&mut buffer)
                .expect("Failed to receive data");
            if buffer == KILL_PACKET {
                continue;
            };

            let peer: HostInfo = rmp_serde::from_slice(&buffer).unwrap();
            let peer = chat::Peer::new(Some(peer.id.clone()), &peer.name, peer_socket);

            let mut peers = state::load_peers().unwrap();
            if peers.contains(&peer) {
                let list_peer = peers
                    .iter_mut()
                    .find(|list_peer| list_peer == &&peer)
                    .unwrap();
                list_peer.socket_addr = peer.socket_addr;
                list_peer.user.name = peer.user.name.clone();
            } else {
                peers.push(peer.clone());
            }
            state::save_peers(&peers).unwrap();
            tauri_app_handle
                .emit_all("new-peer", view_model::ChatInfo::from(peer))
                .unwrap();
        }
    }
}

fn send_discover(socket: UdpSocket) {
    while is_running() {
        let host_info: HostInfo = state::load_host_info().unwrap();
        let serialized_info = rmp_serde::to_vec(&host_info).unwrap();
        socket
            .send_to(
                &serialized_info,
                format!("{}:{}", DISCOVERY_MULTICAST_ADDRESS, DISCOVERY_PORT),
            )
            .expect("Failed to send data");
        thread::sleep(time::Duration::from_secs(5));
    }
}

fn is_running() -> bool {
    unsafe { RUNNING.load(std::sync::atomic::Ordering::Relaxed) }
}

fn set_running(running: bool) {
    unsafe { RUNNING.store(running, std::sync::atomic::Ordering::Relaxed) }
}
