export enum SidebarMode {
    CHAT,
    SETTINGS
}

export default SidebarMode;