import {writable} from "svelte/store";
import type ChatInfo from "./components/chat/ChatInfo";

export let currentChat = writable<ChatInfo | null>(null);