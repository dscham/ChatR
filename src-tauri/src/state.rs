use std::fs::{File, OpenOptions};
use std::io::Write;

use rmp_serde;

use crate::chat::Peer;
use crate::chat::{Message, Peers};
use crate::discovery::HostInfo;

static HOST_INFO_FILE: &str = "host_info.chatrmp";
static PEERS_FILE: &str = "peers.chatrmp";

#[tauri::command]
pub(crate) fn get_host_info() -> Result<HostInfo, String> {
    match load_host_info() {
        Ok(host_info) => Ok(host_info),
        Err(_) => Err("Could not load host info".to_string()),
    }
}

#[tauri::command]
pub(crate) fn save_username(username: String) -> Result<HostInfo, String> {
    let mut host_info;
    if is_first_run() {
        host_info = HostInfo::new(&username);
    } else {
        host_info = load_host_info().unwrap();
        host_info = HostInfo {
            id: host_info.id,
            name: username,
        };
    }

    match save_host_info(&host_info) {
        Ok(_) => Ok(host_info),
        Err(_) => Err("Could not save username".to_string()),
    }
}

#[tauri::command]
pub(crate) fn is_first_run() -> bool {
    load_host_info().is_err()
}

pub(crate) fn load_host_info() -> Result<HostInfo, rmp_serde::decode::Error> {
    let host_info = open_file(HOST_INFO_FILE);
    let host_info = rmp_serde::from_read(host_info);
    host_info
}

pub(crate) fn load_peers() -> Result<Peers, rmp_serde::decode::Error> {
    match load_peers_internal() {
        Ok(peers) => Ok(peers),
        Err(_) => {
            save_peers(&Peers::new()).unwrap();
            load_peers_internal()
        }
    }
}

fn load_peers_internal() -> Result<Peers, rmp_serde::decode::Error> {
    let peers = open_file(PEERS_FILE);
    let peers = rmp_serde::from_read(peers);
    peers
}

pub(crate) fn save_peers(peers: &Peers) -> Result<(), rmp_serde::encode::Error> {
    let mut file = open_file(PEERS_FILE);
    let peers = rmp_serde::to_vec(peers);
    match peers {
        Ok(peers) => {
            write_file(&mut file, peers.as_slice()).unwrap();
            Ok(())
        }
        Err(err) => Err(err),
    }
}

pub(crate) fn get_peer(user_id: &str) -> Option<Peer> {
    let peer = load_peers_internal()
        .unwrap()
        .into_iter()
        .find(|peer| peer.get_user_id() == user_id);
    peer
}

fn save_host_info(host_info: &HostInfo) -> Result<(), rmp_serde::encode::Error> {
    let mut file = open_file(HOST_INFO_FILE);
    let host_info = rmp_serde::to_vec(host_info);
    match host_info {
        Ok(host_info) => Ok(write_file(&mut file, host_info.as_slice()).unwrap()),
        Err(err) => Err(err),
    }
}

fn open_file(path: &str) -> File {
    OpenOptions::new()
        .read(true)
        .write(true)
        .create(true)
        .open(path)
        .expect("Could not open file")
}

fn write_file(file: &mut File, data: &[u8]) -> Result<(), std::io::Error> {
    match file.write_all(data) {
        Ok(_) => Ok(()),
        Err(e) => Err(e),
    }
}

pub(crate) fn save_message(message: &Message) {
    let mut peers = load_peers_internal().unwrap();
    let peer = peers
        .iter_mut()
        .find(|peer| peer.get_user_id() == message.get_user_id())
        .expect("Could not find peer");
    peer.push_message(&message);
    save_peers(&peers).unwrap();
}

pub(crate) fn get_last_message(peer: &Peer) -> Option<Message> {
    let peers = load_peers_internal().unwrap();
    let peer = peers
        .iter()
        .find(|p| p.get_user_id() == peer.get_user_id())
        .expect("Could not find peer");
    peer.get_last_message()
}
