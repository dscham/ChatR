export default interface User {
    id: string;
    name: string;
    lastSeen?: Date
}