use crate::{chat, state};
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub(crate) struct User {
    pub id: String,
    pub name: String,
    pub last_seen: u64,
}

impl User {
    pub(crate) fn from(user: chat::User) -> User {
        User {
            id: user.id,
            name: user.name,
            last_seen: user.last_seen,
        }
    }
}

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub(crate) struct ChatInfo {
    pub user: User,
    pub last_message: Option<chat::Message>,
}

impl ChatInfo {
    pub(crate) fn from(peer: chat::Peer) -> ChatInfo {
        ChatInfo {
            user: User::from(peer.user.clone()),
            last_message: state::get_last_message(&peer),
        }
    }
}
