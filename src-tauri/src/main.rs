#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

use nanoid::nanoid;
use tauri::Manager;

mod chat;
mod discovery;
mod state;
mod view_model;

fn main() {
    tauri::Builder::default()
        .setup(|app| {
            chat::start_message_listener(app.app_handle());
            discovery::start(app.app_handle());
            Ok(())
        })
        .invoke_handler(tauri::generate_handler![
            state::get_host_info,
            state::save_username,
            state::is_first_run,
            chat::send_message,
            chat::get_messages,
            chat::get_chat_infos,
            chat::get_chat_info,
            chat::get_user,
            get_new_nanoid,
        ])
        .build(tauri::generate_context!())
        .expect("error while building tauri application")
        .run(|_app_handle, run_event| {
            if let tauri::RunEvent::WindowEvent {
                label,
                event: tauri::WindowEvent::CloseRequested { .. },
                ..
            } = run_event
            {
                if label == "main" {
                    discovery::stop();
                    chat::stop_message_listener();
                    println!("Threads killed. Closing app.");
                }
            }
        });
}

#[tauri::command]
fn get_new_nanoid() -> String {
    nanoid!(6)
}
