use chrono;
use nanoid::nanoid;
use rmp_serde;
use serde::{Deserialize, Serialize};
use std::net::{Ipv4Addr, SocketAddr, UdpSocket};
use std::sync::atomic::AtomicBool;
use std::thread;
use tauri::Manager;

use crate::state::get_peer;
use crate::view_model::ChatInfo;
use crate::{state, view_model};

const LISTEN_ANY: &str = "0.0.0.0";
const CHAT_PORT: u16 = 42024;
const DUMMY_PORT: u16 = 42042;
const KILL_PACKET: &[u8; 128] = &[0; 128];

static mut LISTENER_RUNNING: AtomicBool = AtomicBool::new(false);

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub(crate) struct User {
    pub id: String,
    pub name: String,
    pub last_seen: u64,
    pub messages: Vec<Message>,
}

impl User {
    pub(crate) fn new(id: Option<String>, name: &str) -> User {
        User {
            id: id.unwrap_or_else(|| nanoid!(6)),
            name: name.to_string(),
            last_seen: chrono::Local::now().timestamp().unsigned_abs(),
            messages: vec![],
        }
    }

    // pub(crate) fn split_for_view(&self) -> (view_model::User, Vec<Message>) {
    //     (view_model::User::from(self.clone()), self.messages.clone())
    // }

    pub(crate) fn messages_mut(&mut self) -> &mut Vec<Message> {
        &mut self.messages
    }
}

impl PartialEq for User {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub(crate) struct Message {
    pub id: String,
    pub user_id: String,
    pub message_type: MessageType,
    pub content_type: ContentType,
    pub created: u64,
    pub content: String,
}

impl Message {
    pub(crate) fn get_user_id(&self) -> String {
        self.user_id.clone()
    }
}

impl PartialEq for Message {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub(crate) enum MessageType {
    RECEIVED,
    SENT,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub(crate) enum ContentType {
    TEXT,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub(crate) struct Peer {
    pub user: User,
    pub socket_addr: SocketAddr,
}

impl Peer {
    pub(crate) fn get_last_message(&self) -> Option<Message> {
        self.user.messages.last().cloned()
    }
}

impl PartialEq for Peer {
    fn eq(&self, other: &Self) -> bool {
        self.user == other.user && self.socket_addr == other.socket_addr
    }
}

impl Peer {
    pub(crate) fn new(id: Option<String>, name: &str, socket_addr: SocketAddr) -> Peer {
        Peer {
            user: User::new(id, name),
            socket_addr,
        }
    }

    pub(crate) fn get_user_id(&self) -> String {
        self.user.id.clone()
    }

    pub(crate) fn push_message(&mut self, message: &Message) {
        self.user.messages_mut().push(message.clone());
    }
}

pub(crate) type Peers = Vec<Peer>;

#[tauri::command]
pub(crate) fn get_user(user_id: &str) -> Option<view_model::User> {
    match get_peer(user_id) {
        Some(peer) => Some(view_model::User::from(peer.user)),
        None => None,
    }
}

#[tauri::command]
pub(crate) fn send_message(mut message: Message) {
    if message.content == "" {
        return;
    }

    let peer = get_peer(&message.user_id).unwrap();
    let socket = UdpSocket::bind(format!(
        "{}:{}",
        peer.socket_addr.ip().to_string(),
        DUMMY_PORT
    ))
    .unwrap();
    message.message_type = MessageType::SENT;
    let message_bytes = rmp_serde::to_vec(&message).unwrap();
    state::save_message(&message);
    socket
        .send_to(
            &message_bytes,
            format!("{}:{}", peer.socket_addr.ip().to_string(), CHAT_PORT),
        )
        .unwrap();
}

#[tauri::command]
pub(crate) fn get_messages(user_id: &str) -> Vec<Message> {
    let messages = get_peer(user_id).unwrap().user.messages;
    messages
}

#[tauri::command]
pub(crate) fn get_chat_infos() -> Vec<ChatInfo> {
    let peers = state::load_peers().unwrap();
    peers
        .iter()
        .map(|peer| ChatInfo::from(peer.clone()))
        .collect()
}

#[tauri::command]
pub(crate) fn get_chat_info(user_id: &str) -> ChatInfo {
    get_peer(user_id).map(|peer| ChatInfo::from(peer)).unwrap()
}

pub(crate) fn start_message_listener<T: tauri::Runtime>(tauri_app_handle: tauri::AppHandle<T>) {
    if !is_listener_running() {
        set_listener_running(true);

        let socket = UdpSocket::bind(format!("{}:{}", LISTEN_ANY, CHAT_PORT)).unwrap();

        thread::spawn(move || message_listener(socket, tauri_app_handle));
    }
}

pub(crate) fn stop_message_listener() {
    if is_listener_running() {
        set_listener_running(false);
        send_kill();
    }
}

fn message_listener<T: tauri::Runtime>(socket: UdpSocket, tauri_app_handle: tauri::AppHandle<T>) {
    while is_listener_running() {
        let mut buffer = [0; 1024];
        if let Ok((size, _peer_socket)) = socket.peek_from(&mut buffer) {
            let mut buffer = vec![0; size];
            socket.recv_from(&mut buffer).unwrap();
            if buffer == KILL_PACKET {
                continue;
            };

            let mut message: Message = rmp_serde::from_slice(&buffer).unwrap();
            message.message_type = MessageType::RECEIVED;
            message.created = chrono::Local::now().timestamp().unsigned_abs();

            state::save_message(&message);
            tauri_app_handle.emit_all("new-message", message).unwrap();
        }
    }
}

fn send_kill() {
    let socket = UdpSocket::bind(format!("{}:{}", Ipv4Addr::LOCALHOST, DUMMY_PORT)).unwrap();
    socket
        .connect(format!("{}:{}", Ipv4Addr::LOCALHOST, CHAT_PORT))
        .unwrap();
    socket.send(KILL_PACKET).unwrap();
}

fn is_listener_running() -> bool {
    unsafe { LISTENER_RUNNING.load(std::sync::atomic::Ordering::Relaxed) }
}

fn set_listener_running(running: bool) {
    unsafe { LISTENER_RUNNING.store(running, std::sync::atomic::Ordering::Relaxed) }
}
