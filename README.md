# ChatR
ChatR is a p2p, LAN chat application. Written in Rust and Svelte on Tauri.

## Features
 - Finds other running ChatR's in the LAN, via UDP Multicast
 - List found ChatR users in the GUI
 - 1to1 Chat

## Possible Features
 - Encryption

## Sockets Needed
 - 0.0.0.0:42690 UDP < for p2p discovery
 - 0.0.0.0:42024 UDP < for chat messages