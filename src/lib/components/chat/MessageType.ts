enum MessageType {
    RECEIVED = "RECEIVED",
    SENT = "SENT",
}

export default MessageType;